import axios from 'axios';

export async function fetchPhotos(tags = 'jack daniels') {
  const params = {
    method: 'flickr.photos.search',
    api_key: '636e1481b4f3c446d26b8eb6ebfe7127',
    per_page: 24,
    format: 'json',
    nojsoncallback: 1,
    sort: 'relevance',
    tags,
  };

  const { data } = await axios.get('https://api.flickr.com/services/rest/', {
    params,
  });

  return data.photos.photo.map((photo) => {
    const { id, title, farm, server, secret } = photo;
    return {
      id,
      title,
      url: `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`,
    };
  });
}
