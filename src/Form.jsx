import { useState } from 'react';

function Form({ onSubmit }) {
  const [text, setText] = useState('');

  function handleSubmit(event) {
    event.preventDefault();
    onSubmit({ text });
  }

  return (
    <form onSubmit={handleSubmit} className="search-form">
      <input
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Search..."
        aria-label="Search for image"
      />
      <button disabled={!Boolean(text)}>Search</button>
    </form>
  );
}

export default Form;
