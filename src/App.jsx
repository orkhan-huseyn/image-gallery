import { useEffect, useState } from 'react';
import Form from './Form';
import Gallery from './Gallery';
import { fetchPhotos } from './utils';

function App() {
  const [photos, setPhotos] = useState([]);

  useEffect(() => {
    async function f() {
      const photos = await fetchPhotos();
      setPhotos(photos);
    }
    f();
  }, []);

  async function handleSubmit({ text }) {
    const photos = await fetchPhotos(text);
    setPhotos(photos);
  }

  return (
    <div className="container">
      <h1>Image Gallery</h1>
      <Form onSubmit={handleSubmit} />
      <div className="tag-list"></div>
      <Gallery photos={photos} />
    </div>
  );
}

export default App;
