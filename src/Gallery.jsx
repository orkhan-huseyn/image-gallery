function Gallery({ photos }) {
  return (
    <ul className="image-list">
      {photos.map((photo) => (
        <li key={photo.id}>
          <img src={photo.url} alt={photo.title} />
        </li>
      ))}
    </ul>
  );
}

export default Gallery;
